const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, options) => {
    const isDevMode = options.mode === "development";
    return {
    watch: false,
    entry: './public/src/index.jsx',
    output: {
        path: path.join(__dirname, 'public/dist'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!sass-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg|ico)$/i,
                use: [
                    "file-loader"
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.', '.js', '.jsx']
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/src/index.html'
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000,
        historyApiFallback: true
    }
}
}