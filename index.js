const PORT = 9090;
var express = require('express');
var app = express();
var http = require('http').Server(app);
var fs = require('fs');

var bodyParser = require('body-parser');
var multer = require('multer'); // v1.0.5
var upload = multer();

app.use(bodyParser.json());
app.use(express.static('public/dist'));

app.use('/*', function(req, res, next){
    res.set({
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "*"
    })
    next();
});

app.get('/books', function(req, res){
    fs.readFile('./books.json', function(err, data){
        var books = JSON.parse(data);
        res.json(books);
    });
});

app.post('/books/add', upload.array(), function(req, res){
    var books = JSON.parse(fs.readFileSync('books.json'));
    
    

    books.push({...req.body});

    fs.writeFile('books.json', JSON.stringify(books, null, 4) ,function(data, err){
        if(err){
            console.error("could not write object: ", err);
        }else{
            console.log(`object ${req.body.id} written to file`);
        }
    })

    console.log(req.body);
    res.json({
        status: 200
    });
});

app.post('/books/delete/:id', upload.array(), function(req, res){
    var books = JSON.parse(fs.readFileSync('books.json'));
    
    var myIndex = books.findIndex(elem => (elem.id.toString() == req.params.id));

    req.body.id = parseInt(req.body.id);

    books.splice(myIndex, 1) ;

    fs.writeFile('books.json', JSON.stringify(books, null, 4) ,function(data, err){
        if(err){
            console.error("could not write object: ", err);
        }else{
            console.log(`object ${req.body.id} removed from file`);
        }
    })

    console.log(req.body);
    res.json({
        status: 200
    });
});


app.post('/books/:id', upload.array(), function(req, res){
    var books = JSON.parse(fs.readFileSync('books.json'));
    
    var myIndex = books.findIndex(elem => (elem.id.toString() == req.params.id));

    req.body.id = parseInt(req.body.id);

    books[myIndex] = {...req.body};

    fs.writeFile('books.json', JSON.stringify(books, null, 4) ,function(data, err){
        if(err){
            console.error("could not write object: ", err);
        }else{
            console.log(`object ${req.params.id} written to file`);
        }
    })

    console.log(req.body);
    res.json({
        status: 200
    });
});

http.listen(PORT, function(){
    console.log(`listening on *:${PORT}`);
});