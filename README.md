# Versa Library

Functionality:
  - Display table data
  - Edit, delete, add new rows
  - Served by default on http://localhost:9090/

# 1. Install dependencies
```sh
$ npm install
```
# 2. Serve app
```sh
$ npm run serve
```
# 3. Open Browser
Open http://localhost:9090/