import React, {Component} from 'react';
import ReactDOM from 'react-dom';

// redux stuff
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import {createLogger} from 'redux-logger';
import allReducers from './reducers';
import fetch from 'cross-fetch';

// components
import App from './components/App';

const logger = createLogger();


fetch("/books")
.then(response => response.json())
.then(function(data){
    const store = createStore(
        allReducers, {},
        applyMiddleware(thunk, promise, logger)
    );

    store.dispatch({
        type: "LOAD_BOOKS",
        payload: data
    });

    ReactDOM.render(
        <Provider store={store} >
            <App />
        </Provider>, document.getElementById('app'));

}).catch(err => console.error(err));