import React, {Component} from 'react';

class BookRowItem extends Component {
    constructor(props){
        super(props);
    }

    render (){
        console.log(this.refs);
        return (
            <td className={
                this.props.editable ? "editable" : ""
            } >
                {
                    !this.props.editable ?
                    (<span>
                        {this.props.val}
                    </span>) :
                    (<input type="text" defaultValue={this.props.val} />)
                }
            </td>
        )
    }
}

export default BookRowItem;