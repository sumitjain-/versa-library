import React, {Component} from 'react';
import {connect} from 'react-redux';

import BookRow from './BookRow';
import NewBookForm from './NewBookForm';

class App extends Component {
    constructor(props){
        super(props);
    }
    render (){
        return (
            <div>
                <h1>
                    Versa Library
                </h1>
                <table className="table">
                    <thead>
                        <tr>
                            {/* <th>Book ID</th> */}
                            {
                                this.props.properties.map(function(property){
                                    if(property.key != "id"){
                                        return (
                                            <th key={property.key} >
                                                {property.value}
                                            </th>
                                        )
                                    }
                                })
                            }
                            <th>Functions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {/* <tr>
                            <td>aa</td>
                            <td>sd</td>
                            <td>ada</td>
                            <td>sfs</td>
                            <td>sfd</td>
                        </tr> */}

                        {
                            this.props.books.map(function(book) {
                                return (
                                    <BookRow key={book.id} book={book} />
                                )
                            })
                        }
                    </tbody>
                    <NewBookForm />
                </table>
            </div>
        )
    }
}

function mapStateToProps (state){
    return {
        books: state.books,
        properties: state.properties
    }
}

export default connect(mapStateToProps)(App);