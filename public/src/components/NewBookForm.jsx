import React, {Component} from 'react';
import { connect } from 'react-redux';
import addBook from '../actions/addBook';

class NewBookForm extends Component {
    constructor(props){
        super(props);
        this.addBook = this.addBook.bind(this);
    }
    addBook(e){
        e.preventDefault();
        var self = this;
        var obj = {};
        if(this.props.books.length){
            obj.id = parseInt(self.props.books[self.props.books.length - 1].id) + 1;
        }else{
            obj.id = 1;
        }

        this.props.properties.forEach(property => {
            if(property.key != "id"){
                obj[property.key] = self.refs[property.key].value;
            }
        })


        fetch("/books/add" , {
            method: "POST",
            headers: {
                "accepts": "application/json",
                "content-type": "application/json;  charset=utf-8"
            },
            body: JSON.stringify(obj)
        }).then(res => res.json())
        .then(function(data){
            console.log("dispatching action");
            self.props.addBook(obj);
        })
        .catch(err => console.error("Post failed: ", err));
    }
    render(){
        return (
                    <tfoot>
                        <tr className="form-group" >
                            {
                                this.props.properties.map(function(property){
                                    var input_type = "";
                                    switch (property.key){
                                        case "price":
                                            input_type = "number";
                                            break;
                                        case "pub_date":
                                            input_type = "date";
                                            break;
                                        default:
                                            input_type = "text";
                                            break;
                                    }
                                    if(property.key != "id"){
                                        return (
                                            <td key={property.key} ><input className="form-control" required ref={property.key} name={property.key} type={input_type} placeholder={property.value} /></td>
                                        )
                                    }
                                })
                            }
                            <td><button className="btn btn-success" onClick={this.addBook} >Add</button></td>
                        </tr>
                    </tfoot>
        )
    };
}


function mapStateToProps (state){
    return {
        books: state.books,
        properties: state.properties
    }
}

function mapDispatchToProps (dispatch){
    return {
        addBook: (obj) => dispatch(addBook(obj))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewBookForm);