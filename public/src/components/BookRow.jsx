import React, {Component} from 'react';
import {connect} from 'react-redux';
import deleteBook from '../actions/deleteBook';
import editBook from '../actions/editBook';

class BookRow extends Component {
    constructor(props){
        super(props);
        this.state = {
            editable: false
        };

        this.toggleEditable = this.toggleEditable.bind(this);
        this.saveBookDetails = this.saveBookDetails.bind(this);
        this.renderRowItem = this.renderRowItem.bind(this);
    }
    toggleEditable(){
        var tempState = this.state;
        this.setState({
            editable: !this.state.editable
        });
    }

    saveBookDetails() {
        var obj = {};
        var self = this;
        Object.keys(this.refs).forEach(function(key){
            obj[key] = self.refs[key].value;
        })
        this.props.properties.forEach(function(property){
            if(property.key != "id"){
                obj[property.key] = self.refs[property.key].value;
            }else{
                obj.id = self.refs.id.value;
            }
        })
        fetch("/books/"+obj.id , {
            method: "POST",
            headers: {
                "accepts": "application/json",
                "content-type": "application/json;  charset=utf-8"
            },
            body: JSON.stringify(obj)
        }).then(res => res.json())
        .then(function(data){
            self.props.editBook(obj);
        })
        .catch(err => console.error("Post failed: ", err));
        this.toggleEditable();
    }
    deleteRow(){
        var self = this;
        console.log(`deleting ${self.props.book}`);
        var myId = self.props.book.id;
        
        
        var confirmation = confirm(`All data for book ${this.props.book.name} will be
         permanently lost. Are you sure you want to proceed?`);

        if(!confirmation){
            return;
        }
        fetch("/books/delete/"+myId , {
            method: "POST",
            headers: {
                "accepts": "application/json",
                "content-type": "application/json;  charset=utf-8"
            },
            body: JSON.stringify({})
        }).then(res => res.json())
        .then(function(data){
            self.props.deleteBook(myId);
        })
        .catch(err => console.error("Post failed: ", err));
    }

    renderRowItem(book_property, val){
        var input_type = "";
        switch (book_property){
            case "price":
                input_type = "number";
                break;
            default:
                input_type = "text";
                break;
        }
        return (
            <td style={{
                "display": ( book_property == "id" ? "none" : "auto" )
            }} key={book_property} className={
                this.state.editable ? "editable" : ""
            } >
                {
                    !this.state.editable ?
                    (<span>
                        {val || (<em>not found</em>)}
                    </span>) :
                    (<input className="form-control" type={input_type} readOnly={book_property == "id"} ref={book_property} defaultValue={val} />)
                }
            </td>
        )
        
    }
    render (){
        
        var self = this;
        return (
            <tr className="bookRow" >
                {
                    Object.keys(this.props.book).map(function(book_property){
                        return self.renderRowItem(book_property, self.props.book[book_property]);
                    })
                }
                <td>
                {
                    this.state.editable ?
                    (<button className="btn btn-success" onClick={() => this.saveBookDetails()} >Save</button>) :
                    (<button className="btn btn-primary" onClick={() => this.toggleEditable()} >Edit</button>)
                }
                {
                    this.state.editable ?
                    (<span></span>) :
                    (<button className="btn btn-danger" onClick={() => this.deleteRow()} >Delete</button>)
                }
                </td>
            </tr>
        )
    }
}

function mapStateToProps (state){
    return {
        properties: state.properties
    }
}

function mapDispatchToProps (dispatch){
    return {
        editBook: (obj) => dispatch(editBook(obj)),
        deleteBook: (id) => dispatch(deleteBook(id))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(BookRow);