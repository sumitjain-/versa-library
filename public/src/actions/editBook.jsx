export default function(payload){
    console.log("editing book in action creator");
    return {
        type: "EDIT_BOOK",
        payload: payload
    }
}