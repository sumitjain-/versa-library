export default function(id){
    console.log("editing book in action creator");
    return {
        type: "DELETE_BOOK",
        payload: id
    }
}