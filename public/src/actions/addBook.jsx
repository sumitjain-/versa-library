export default function(payload){
    console.log("adding book in action creator");
    return {
        type: "ADD_BOOK",
        payload: payload
    }
}