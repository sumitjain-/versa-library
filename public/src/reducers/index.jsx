import {combineReducers} from 'redux';
import booksReducer from './reducer-books';
import propertiesReducer from './reducer-properties';

const allReducers = combineReducers({
    books: booksReducer,
    properties: propertiesReducer
});

export default allReducers;