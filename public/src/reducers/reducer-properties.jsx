export default function (state=[], action){
    return [
        {
            "key":"id", "value":"ID"
        },
        {
            "key":"name", "value":"Name"
        },
        {
            "key":"author", "value":"Author"
        },
        {
            "key":"publisher",  "value":"Publisher"
        },
        {
            "key":"price",  "value":"Price"
        },
        {
            "key":"pub_date",  "value":"Publish Date"
        }
    ]
}