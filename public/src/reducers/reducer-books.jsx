export default function (state=[], action){
    switch(action.type){
        case "LOAD_BOOKS":
            state = [
                ...action.payload
            ]
            return state;
        
        case "EDIT_BOOK":
            console.log("editing book");
            var myIndex = state.findIndex(elem => (elem.id == action.payload.id));
            console.log(`index found at ${myIndex}`);
            var tempState = [...state];
            tempState[myIndex] = action.payload;
            return tempState;
        
        case "ADD_BOOK":
            var tempState = [...state];
            tempState.push(action.payload);
            return tempState;
        
        case "DELETE_BOOK":
            var tempState = [...state];
            var myIndex = tempState.findIndex(elem => (elem.id == action.payload));

            tempState.splice(myIndex, 1) ;
            return tempState;
        default:
            return state;
    }
}